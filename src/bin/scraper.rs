use regex::Regex;
use std::fs::File;
use std::io::Write;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = reqwest::blocking::Client::new();
    let res = client
        .get("https://www.youtube.com/results?search_query=nothing+else+matters+metallica")
        .send()?;
    let search_results_html = res.text().unwrap();
    // Copied this regex from this brilliant library:
    // https://github.com/HermanFassett/youtube-scrape/blob/master/scraper.js#L44
    let re = Regex::new("ytInitialData = [^{]*(.*?); *</script>").unwrap();
    for cap in re.captures_iter(&search_results_html) {
        let json_str = cap[0]
            // Obviously, the NodeJS based regex doesn't port over to Rust with identical behaviour,
            // so some additional find-and-replaces are necessary to get the JSON to parse properly.
            .replace("ytInitialData = ", "")
            .replace(";</script>", "");
        // Adding a type annotation of serde_json::Value to bypass Rustc complaining about a lack of typing info.
        let json: serde_json::Value = serde_json::from_str(&json_str).unwrap();
        let mut f = File::create("yt.json")?;
        f.write_all(json_str.as_bytes())?;
    }
    Ok(())
}
