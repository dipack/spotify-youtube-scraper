use actix_web::{get, web, App, HttpServer};
use serde::Deserialize;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;

const LOCALHOST_ADDR: &str = "127.0.0.1:4001";
const STORAGE_FILE_PATH: &str = "/home/dipack/projects/spotify-youtube-scraper-rust/data/token.txt";

#[derive(Deserialize)]
struct CallbackInfo {
    code: String,
}

#[get("/spotify/callback")]
async fn callback(info: web::Query<CallbackInfo>) -> Result<String, actix_web::Error> {
    println!("CALLBACK WAS CALLED: {:#?}", info.code);
    let file_path = Path::new(STORAGE_FILE_PATH);
    fs::create_dir_all(file_path.parent().unwrap())?;
    let mut file = File::create(file_path)?;
    file.write_all(info.code.as_bytes())?;
    Ok(format!(
        "Code was returned {} - wrote to file {}",
        info.code, STORAGE_FILE_PATH
    ))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Starting Oauth callback server at {}", LOCALHOST_ADDR);
    HttpServer::new(|| App::new().service(callback))
        .bind(LOCALHOST_ADDR)?
        .run()
        .await
}
