use percent_encoding::{percent_encode, AsciiSet, CONTROLS};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::thread;
use std::time;

const CONFIG_FILE_PATH: &str = "/home/dipack/projects/spotify-youtube-scraper-rust/config.toml";
const REDIRECT_URI: &str = "http://localhost:4001/spotify/callback";
const MAX_ATTEMPTS: u32 = 5;

#[derive(Serialize, Deserialize, Debug)]
struct ClientApplicationConfig {
    client_id: String,
    client_secret: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ApplicationPaths {
    token: String,
    oauth_creds: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ApplicationConfig {
    client_app: ClientApplicationConfig,
    paths: ApplicationPaths,
}

impl ApplicationConfig {
    fn read_config(path: &str) -> Option<ApplicationConfig> {
        let mut file = File::open(path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).ok()?;
        Some(toml::from_str(&contents[..]).unwrap())
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct OauthCredentialsResponse {
    access_token: String,
    token_type: String,
    refresh_token: String,
    scope: String,
    expires_in: u32,
}

#[derive(Serialize, Deserialize, Debug)]
struct OauthRefreshResponse {
    access_token: String,
    token_type: String,
    scope: String,
    expires_in: u32,
}

#[derive(Deserialize, Debug, Clone)]
struct Artist {
    name: String,
    href: String,
    id: String,
}

#[derive(Deserialize, Debug, Clone)]
struct Track {
    name: String,
    href: String,
    id: String,
    popularity: u32,
    track_number: u32,
    duration_ms: u64,
    artists: Vec<Artist>,
}

#[derive(Deserialize, Debug, Clone)]
struct TrackItem {
    added_at: String,
    track: Track,
}

#[derive(Deserialize, Debug)]
struct SavedTracksResponse {
    href: String,
    limit: u32,
    next: Option<String>,
    offset: u32,
    previous: Option<String>,
    total: u32,
    items: Vec<TrackItem>,
}

/// Attempt to read from the given path a maximum of 5 times, sleeping for duration milliseconds between attempts.
fn wait_and_read(path: &str, duration: u64) -> Option<String> {
    let mut counter = 0u32;
    let file_path = Path::new(path);
    while counter < MAX_ATTEMPTS {
        if file_path.exists() {
            let mut file = File::open(file_path).ok()?;
            let mut token = String::new();
            file.read_to_string(&mut token).ok()?;
            return Some(token.clone());
        } else {
            counter += 1;
            let sleep_duration = time::Duration::from_millis(duration);
            println!("Sleeping for {} ms", sleep_duration.as_millis());
            thread::sleep(sleep_duration);
        }
    }
    None
}

/// Fetch, refresh, and save OAuth credentials if they exist, at the given path.
fn fetch_and_refresh_oauth_credentials_if_exists(
    config: &ApplicationConfig,
    client: &reqwest::blocking::Client,
) -> Option<OauthCredentialsResponse> {
    let save_path = Path::new(&config.paths.oauth_creds);
    if save_path.exists() {
        let credentials: OauthCredentialsResponse =
            serde_json::from_reader(File::open(save_path).ok()?).ok()?;

        let auth_header = format!(
            "Basic {}",
            base64::encode(format!(
                "{}:{}",
                config.client_app.client_id, config.client_app.client_secret
            ))
        );

        let refresh_response = client
            .post("https://accounts.spotify.com/api/token")
            .header("Authorization", auth_header)
            .form(&[
                ("grant_type", "refresh_token"),
                ("refresh_token", &credentials.refresh_token[..]),
            ])
            .send()
            .ok()?;
        let refreshed = refresh_response.json::<OauthRefreshResponse>().unwrap();

        let updated_credentials = OauthCredentialsResponse {
            refresh_token: credentials.refresh_token.clone(),
            access_token: refreshed.access_token,
            expires_in: refreshed.expires_in,
            token_type: refreshed.token_type,
            scope: refreshed.scope,
        };
        let file = File::create(save_path).ok()?;
        serde_json::to_writer(file, &updated_credentials).ok()?;

        return Some(updated_credentials);
    }
    return None;
}

/// First time fetch and save OAuth credentials at the given path.
fn fetch_and_save_oauth_credentials(
    code: &str,
    config: &ApplicationConfig,
    client: &reqwest::blocking::Client,
) -> Option<OauthCredentialsResponse> {
    let auth_header = format!(
        "Basic {}",
        base64::encode(format!(
            "{}:{}",
            config.client_app.client_id, config.client_app.client_secret
        ))
    );
    let response = client
        .post("https://accounts.spotify.com/api/token")
        .header("Authorization", auth_header)
        .form(&[
            ("grant_type", "authorization_code"),
            ("code", code),
            ("redirect_uri", REDIRECT_URI),
        ])
        .send()
        .ok()?;
    let credentials = response.json::<OauthCredentialsResponse>().unwrap();

    let save_path = Path::new(&config.paths.oauth_creds);
    let file = File::create(save_path).ok()?;
    serde_json::to_writer(file, &credentials).ok()?;

    return Some(credentials);
}

/// Query Spotify to get all of the user's saved tracks.
fn get_user_saved_tracks(
    access_token: &str,
    client: &reqwest::blocking::Client,
) -> Result<Vec<Track>, Box<dyn std::error::Error>> {
    let limit: u32 = 50;
    let mut offset: u32 = 0;
    let mut tracks = Vec::new();
    loop {
        let response = client
            .get("https://api.spotify.com/v1/me/tracks")
            .query(&[("limit", limit), ("offset", offset)])
            .header("Authorization", format!("Bearer {}", access_token))
            .send()?;
        let json = response.json::<SavedTracksResponse>()?;
        // This took any annoying amount of time to figure out..
        // Basically, we need to copy a mutable reference from the JSON array,
        // and then also clone the items in the JSON we want to copy into our vector.
        // Finally, we use the `collect()` function to coerce the values into a Vector.
        tracks.append(
            &mut json
                .items
                .iter()
                .map(|track| track.track.clone())
                .collect::<Vec<Track>>(),
        );
        if json.next.is_some() {
            offset += limit;
        } else {
            break;
        }
    }
    Ok(tracks)
}

const FRAGMENT: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');

/// Scrape Youtube, and return the video ID for the top hit for a given string query.
fn scrape_youtube(
    query: &str,
    client: &reqwest::blocking::Client,
) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let res = client
        .get(format!(
            "https://www.youtube.com/results?search_query={}",
            percent_encode(query.as_bytes(), FRAGMENT)
        ))
        .send()?;
    let search_results_html = res.text().unwrap();
    // Copied this regex from this brilliant library:
    // https://github.com/HermanFassett/youtube-scrape/blob/master/scraper.js#L44
    let re = Regex::new("ytInitialData = [^{]*(.*?); *</script>").unwrap();
    for cap in re.captures_iter(&search_results_html) {
        let json_str = cap[0]
            // Obviously, the NodeJS based regex doesn't port over to Rust with identical behaviour,
            // so some additional find-and-replaces are necessary to get the JSON to parse properly.
            .replace("ytInitialData = ", "")
            .replace(";</script>", "");
        // Adding a type annotation of serde_json::Value to bypass Rustc complaining about a lack of typing info.
        let json: serde_json::Value = serde_json::from_str(&json_str).unwrap();
        return Ok(Some(String::from(
            json.get("contents")
                .ok_or("Expected to find a key \"contents\"")?
                .get("twoColumnSearchResultsRenderer")
                .ok_or("Expected to find a key \"twoColumnSearchResultsRenderer\"")?
                .get("primaryContents")
                .ok_or("Expected to find a key \"primaryContents\"")?
                .get("sectionListRenderer")
                .ok_or("Expected to find a key \"sectionListRenderer\"")?
                .get("contents")
                .ok_or("Expected to find a key \"contents\"")?[0]
                .get("itemSectionRenderer")
                .ok_or("Expected to find a key \"itemSectionRenderer\"")?
                .get("contents")
                .ok_or("Expected to find a key \"contents\"")?[0]
                .get("videoRenderer")
                .ok_or("Expected to find a key \"videoRenderer\"")?
                .get("videoId")
                .ok_or("Expected to find a key \"videoId\"")?
                .as_str()
                .ok_or("Expected to be able to convert to str")?,
        )));
    }
    Ok(None)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config =
        ApplicationConfig::read_config(CONFIG_FILE_PATH).ok_or("Failed to read config file")?;

    let client = reqwest::blocking::Client::new();
    // Get OAuth credentials.
    let credentials = match fetch_and_refresh_oauth_credentials_if_exists(&config, &client) {
        // If we already have some lying around, refresh, persist, and use those.
        Some(oauth_creds) => oauth_creds,
        // .. else, get a new set and persist them.
        None => {
            let first_stage = client
                .get("https://accounts.spotify.com/authorize")
                .query(&[
                    ("client_id", &config.client_app.client_id[..]),
                    ("response_type", "code"),
                    ("redirect_uri", REDIRECT_URI),
                    ("scope", "user-library-read"),
                ])
                .send()?;
            let url = format!(
                "https://{}{}?{}",
                first_stage.url().host().unwrap(),
                first_stage.url().path(),
                first_stage.url().query().unwrap_or("")
            );
            open::that(url).unwrap();
            let token = wait_and_read(&config.paths.token, 2000u64).unwrap();

            fetch_and_save_oauth_credentials(&token, &config, &client).unwrap()
        }
    };

    // Get tracks from Spotify.
    let tracks = get_user_saved_tracks(&credentials.access_token, &client).unwrap();

    for track in tracks {
        // Scrape youtube to get the top-hit for the track title + artist name.
        let video_id = scrape_youtube(
            &format!("{} {}", track.name, track.artists[0].name),
            &client,
        )
        .unwrap()
        .unwrap();
        println!(
            "Video URL for {} by {} is {}",
            track.name,
            track.artists[0].name,
            format!("https://youtube.com/watch?v={}", video_id)
        );
    }
    Ok(())
}
