# YouTube scraper for Spotify Saved Tracks

A little application I wrote to scrape YouTube for songs in my Spotify Saved Tracks, as a way to learn Rust.

Blog post chronicling the journey, in brief: https://dipack.dev/you-tube-scraper-for-your-spotify-library-in-rust

Project structure adopted from https://doc.rust-lang.org/cargo/guide/project-layout.html